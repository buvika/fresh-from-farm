// create API using NodeJS

// import http object
var http = require('http');

// req = request caries data from client to server
// res = response carries data from server to client
var myServer = http.createServer(function(req, res){
    res.write('Connection Successfull !!! Welcome to Node Server');
    res.end();
});

myServer.listen(3000);
console.log('Use URL: localhost:3000');